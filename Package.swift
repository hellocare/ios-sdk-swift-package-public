// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription
public var disallowsBinaryDependencies = false
let package = Package(
    name: "HellocareSDK",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "HellocareSDK",
            targets: ["HellocareSDK", "TwilioVideo"]),
    ],
    targets: [
        .binaryTarget(
            name: "HellocareSDK",
            url: "https://github.com/hellocaredev/direct-sdk-ios/releases/download/2.8/HellocareSDK.xcframework.zip",
            checksum: "5ad0a0678af8d3da2b86d83c863621e4eff23c9d46b58206a9f6b36223d32467"
        ),
        .binaryTarget(
            name: "TwilioVideo",
            url: "https://github.com/twilio/twilio-video-ios/releases/download/4.0.0-beta3/TwilioVideo.xcframework.zip",
            checksum: "e2b14f83462e6afcf7921b445da18a58f116aa9cb80308ec15739b55099c32fd"
        )
    ]
)
